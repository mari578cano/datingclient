import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// Modules
import {ToastrModule} from 'ngx-toastr';
import {TabsModule} from 'ngx-bootstrap/tabs';
import { NgxGalleryModule} from '@kolkov/ngx-gallery';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    TabsModule.forRoot(),
    ToastrModule.forRoot({
      positionClass: 'toast-bottom-right'
    }),
    NgxGalleryModule
  ],
  exports: [
    ToastrModule,
    TabsModule
  ]
})
export class SharedModule { }
