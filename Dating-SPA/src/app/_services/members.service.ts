import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';

// Config
import {environment} from '../../environments/environment';

// RXJS
import {Observable, of} from 'rxjs';

// Models
import {Member} from '../_models/member';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class MembersService {
  baseUrl = environment.apiUrl;
  members: Member[] = [];

  constructor(private http: HttpClient) { }

  public getMembers(): Observable<Member[]> {
    if (this.members.length > 0) { return  of(this.members); }
    return this.http.get<Member[]>(this.baseUrl + 'users').pipe(
      map(members => {
        this.members = members;
        return members;
      })
    );
  }
  public getMember(username: string): Observable<Member> {
    const member = this.members.find(x => x.username === username);
    if (member !== undefined) { return of(member); }
    return this.http.get<Member>(this.baseUrl + 'users/' + username);
  }
  public updatedMember(member: Member): Observable<void> {
    return this.http.put<string>(this.baseUrl + 'users', member).pipe(
      map(() => {
        const index = this.members.indexOf(member);
        this.members[index] = member;
      })
    );
  }
}
