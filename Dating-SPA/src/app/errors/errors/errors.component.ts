import { Component, OnInit } from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Component({
  selector: 'app-errors',
  templateUrl: './errors.component.html',
  styleUrls: ['./errors.component.css']
})
export class ErrorsComponent implements OnInit {
  baseUrl = 'https://localhost:5001/api';
  validationErrors: string[] = [];

  constructor(private  http: HttpClient) { }

  ngOnInit(): void {
  }

  get400Error(): void {
    this.http.get(this.baseUrl + '/buggy/bad-request').subscribe(response => {
      console.log(response);
    }, error => {
      console.log(error);
    });
  }

  get400ValidationError(): void {
    this.http.post(this.baseUrl + '/account/register', {}).subscribe(response => {
      console.log(response);
    }, error => {
      console.log(error);
      this.validationErrors = error;
    });
  }

  get401Error(): void {
    this.http.get(this.baseUrl + '/buggy/auth').subscribe(response => {
      console.log(response);
    }, error => {
      console.log(error);
    });
  }

  get404Error(): void {
    this.http.get(this.baseUrl + '/buggy/not-found').subscribe(response => {
      console.log(response);
    }, error => {
      console.log(error);
    });
  }

  get500Error(): void {
    this.http.get(this.baseUrl + '/buggy/server-error').subscribe(response => {
      console.log(response);
    }, error => {
      console.log(error);
    });
  }
}
