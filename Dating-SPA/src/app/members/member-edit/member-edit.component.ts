/* tslint:disable:typedef */
import {Component, HostListener, OnInit, ViewChild} from '@angular/core';
import {User} from '../../_models/user';
import {Member} from '../../_models/member';
import {AccountService} from '../../_services/account.service';
import {MembersService} from '../../_services/members.service';
import {take} from 'rxjs/operators';
import {ToastrService} from 'ngx-toastr';
import {NgForm} from '@angular/forms';

@Component({
  selector: 'app-member-edit',
  templateUrl: './member-edit.component.html',
  styleUrls: ['./member-edit.component.css']
})
export class MemberEditComponent implements OnInit {
  @ViewChild('editForm') editForm: NgForm;
  member: Member;
  user: User;
  @HostListener('window:beforeunload', ['$event']) unloadNotification($event: any) {
    if (this.editForm.dirty || this.editForm.value.city !== this.member.city
      || this.editForm.value.country !== this.member.country) {
      $event.returnValue = true;
    }
  }

  constructor(private accountService: AccountService,
              private memberService: MembersService,
              private toastr: ToastrService) {
    this.accountService.currentUser$.pipe(take(1)).subscribe(user => this.user = user );
  }

  ngOnInit(): void {
    this.loadMember();
  }

  loadMember(): void {
    this.memberService.getMember(this.user.userName).subscribe(member => {
      this.member = member;
    });
  }

  updateMember(): void {
    this.memberService.updatedMember(this.member).subscribe(() => {
      this.toastr.success('profile updated success');
      this.editForm.resetForm(this.member);
    });
  }
}
